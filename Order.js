var db = require('./Conexion');
var ms = require('../responses/common.js');
  	
class Order {
	constructor(id){
		this.id = id;
	}

	static async getOrderStatus (){
		let query = `select * from order_status`;
		let statuses = null;
		await db.execute(query,[])
			.then(result => {
				if (result.length > 0){
					statuses = result;
				}
			}).catch(error => {
				// catch error
				console.log(error);
			});
		return statuses;
	}

	static async getUser (userId){
		let query = `select id, type from user where id = ?`;
		let user = null;
		await db.execute(query,[userId])
			.then(result => {
				if (result.length > 0){
					user = result[0];
				}
			}).catch(error => {
				// catch error
				console.log(error);
			});
		return user;
	}

	static async getAllOrders (userId, userType){
		let query = `select 
						o.id,
					    o.created_at as date,
					    user.id as user_id,
					    user.name as username,
					    o.address,
					    o.status,
					    os.description as status_name
					from \`order\` o
					inner join user 
					on user.id = o.user_id
					inner join order_status os
					on os.id = o.status`;
		
		if (userType == 'restaurant') {
			query += ` inner join order_details od
						on od.order_id = o.id
						inner join product p
						on p.id = od.product_id
						where p.owner_id = ?`
		}

		if (userType == 'customer') {
			query += ` where user.id = ?`
		}

		let orders = [];

		await db.execute(query,[userId])
			.then(result => {
				orders = result;
			}).catch(error => {
				// catch error
			});
		return orders;
	}

	static async getOrderData(orderId, userId, userType) {
		let query = `select 
						p.id,
					    p.code,
					    p.name,
					    od.unit_price,
					    od.quantity
					from order_details od
					inner join product p
					on p.id = od.product_id
					where order_id = ?`;
		
		if (userType == 'restaurant') {
			query += ` and p.owner_id = ?`
		}

		let details = [];

		await db.execute(query,[orderId, userId])
			.then(result => {
				details = result;
			}).catch(error => {
				// catch error
			});
		return details;
	}

	static updateOrder (orderId, status){
		let query = `update \`order\` set status = ? where id = ?`;
		return db.execute(query,[status, orderId]);
	}

	static async findUserByEmail(email) {
		let query = `select * from user where lower(email) = lower(?)`;
		let user = null;
		await db.execute(query,[email])
			.then(result => {
				if (result.length > 0) {
					user = result[0];
				}
			}).catch(error => {
				// catch error
				console.log(error)
			});
		return user;
	}

	static async createGuestUser(name, email) {
		let query = `insert into user(email, name, type, is_active ) values (lower(?),?,'customer',0); `;
		let cart = null;

		await db.execute(query, [email, name])
			.then(result => {
				cart = result;
			}).catch(error => {
				console.log(error);
				// catch error
			});
		
		return cart;
	}

	static async createOrder(cartId, details, userId, address){
		let query = `insert into \`order\`(user_id, status, address, created_at) values (?,  1, ?, now()); `;
		let saved = true;

		await db.execute(query, [userId, address])
			.then(result => {}).catch(error => {
				console.log(error);
				saved = false;
				// catch error
			});

		query = `select last_insert_id() as id;`;
		let orderId = 0;


		if (saved) {
			await db.execute(query, [])
				.then(result => {
					orderId = result[0]['id'];
				}).catch(error => {
					console.log(error);
					saved = false;
					// catch error
				});
		}

		if (saved) {
			query = '';
			let params = [];
			for (var i = 0; i < details.length; i++) {
				query += `insert into order_details (order_id, product_id, unit_price, quantity) values (?,?,?,?); `;
				params.push(orderId)
				params.push(details[i].product_id);
				params.push(details[i].price);
				params.push(details[i].quantity);
			}

			await db.execute(query, params)
				.then(result => {}).catch(error => {
					console.log(error);
					saved = false;
					// catch error
				});
		}

		if (saved) {
			query = `update cart set status = 0 where id = ?`;

			await db.execute(query, [cartId ])
				.then(result => {}).catch(error => {
					console.log(error);
					saved = false;
					// catch error
				});
		}
		
		return saved;
	}
}

module.exports = Order;