import { Component } from '@angular/core';
import {AlphaService} from './Services/alpha.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public image: string //image buffer para actualizar el componente de file-upload luego de una 
  //carga exitosa. No es necesario si utilizas la version del file-upload que no tiene imagen.

  constructor(public Alpha:AlphaService) {
    //Imagen inicial. En este caso esta quemada, en la implementacion real deberia 
    //ser inicializada a la imagen actual de lo que querras editar.
    this.image = 'https://ccv-assets.s3.us-east-2.amazonaws.com/wings.jpg'; 
  }

  title = 'Ren-drive';


  refresh($event: {id: number, url: string}) {
    this.image = $event.url;
    console.log(`Successfully uploaded file with id: ${$event.id}`);
    //Esta funcion se llama cuando acaba la subida de la imagen.
    //Entonces es util para llamar en este momento al recurso del backend
    //que se va a encargar de enlazar el recurso (producto, tienda, etc) con la imagen
    //recien subida. Para esto unicamente es necesaria el id de la imagen, la URL
    //solo es para actualizar la imagen actual pero no se utiliza para nada en el backend.
  }
}
