const lambdaUrl = 'https://h6li2mrz2i.execute-api.us-east-2.amazonaws.com/beta'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlphaService {

  constructor(private http: HttpClient) {}

  private request(method: string, url: string, data?: any,response_type:"json"|"text"|"arraybuffer" = 'json') {
    const result = this.http.request(method, url, {
      body: data,
      responseType: response_type,
      observe: 'body'
    });
    return new Promise<any>((resolve, reject) => {
      result.subscribe(resolve as any, reject as any);
    });
  }

  success(message:string){
    if(!message)return;
    alert(message);
  }
  inform(message:any){
    if(!message)return;
    alert(message);
  }
  error(message:any){
    if(!message)return;
    alert(message);
  }

  async registerFile(fbody : object) {
    return await this.request('post',`${lambdaUrl}/files`,fbody);
  }
}
