package ren.errors;

public class DBException extends RuntimeException
{
    public DBException(final String query) {
        super( "Failed query: " + query );
    }
}
