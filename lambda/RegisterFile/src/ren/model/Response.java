package ren.model;

import java.net.URL;

public class Response
{
    public URL url;
    public String key;
    public int id;
    
    public Response() {
    }
    
    public Response(final URL u, final String name, final int id) {
        this.url = u;
        this.key = name;
        this.id = id;
    }
}
