package ren.model;

public class Event
{
    public String nombre;
    public String tipo;
    public int size;
    
    public String getQuery() {
        final String queryParams = String.format("(%s,'%s','%s')", this.size, this.nombre, this.tipo);
        return String.format("insert into fotos(size,nombre,tipo) values %s returning id;", queryParams);
    }
}
